## Running Network Calculations in a Container 

**There is some weirdness linking the Boost libraries required by SkyNet. If they are not explicitly included in the CMake file for this project, it failes to build because the linker can't find the Boost libraries. I have not had a chance to diagnose what is going on.** 

*Requires having built a SkyNet Docker image that contains the SkyNet libraries* 

To start a container that you can run calculations in, copy the contents of this directory to a directory that you want to work in. Then start an interactive Docker container based on the `SkyNet` docker image. 

```Bash 
$ sudo docker run -it -v $(pwd):/r-process/ skynet:1.0 
```
The `-v` option bind mounts the present working directory to the directory `/r-process` in the container. 


Once you are running the Docker container interactively, run the commands below to build the r-process executable and run it. This should produce a `SkyNet` output file in the directory you ran the Docker container from. Rather than building within the main `SkyNet` code, this example uses a new CMake project which links against the `SkyNet` libraries. 

```Bash 
$ cd r-process 
$ mkdir build
$ cd build 
$ cmake -DSkyNet_DIR=[the location of your skynet installation]/lib/cmake/SkyNet/ ..  
$ make -j4 
$ ./r-process 
```

It should be straightforward to use this as a template for more complex calculations using `SkyNet`.