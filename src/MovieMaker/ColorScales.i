%{
#include "MovieMaker/PiecewiseLinearColorScale.hpp"
#include "MovieMaker/ColorScales.hpp"
%}

%include "MovieMaker/PiecewiseLinearColorScale.hpp"
%include "MovieMaker/ColorScales.hpp"