/// \file FlushStdout.hpp
/// \author jlippuner
/// \since Aug 14, 2014
///
/// \brief
///
///

#ifndef SKYNET_UTILITIES_FLUSHSTDOUT_HPP_
#define SKYNET_UTILITIES_FLUSHSTDOUT_HPP_

class FlushStdout {
public:
  static void Flush();
private:
  FlushStdout() {}
};

#endif // SKYNET_UTILITIES_FLUSHSTDOUT_HPP_
