FROM ubuntu:20.04 as skynet-dependencies 
LABEL author="Luke Roberts" 

ARG DEBIAN_FRONTEND=noninteractive
RUN  apt-get update && apt-get install -y \
    build-essential \
    git \ 
    cmake \ 
    gfortran \ 
    libhdf5-dev \
    libgsl-dev \
    libmkl-dev \
    libboost-system-dev \ 
    libboost-filesystem-dev \
    libboost-serialization-dev

FROM skynet-dependencies as skynet
WORKDIR /skynet 
COPY . . 
RUN mkdir build_local && mkdir install && cd build_local \ 
    && cmake -DSKYNET_MATRIX_SOLVER=mkl \
    -DCMAKE_INSTALL_PREFIX=/skynet/install \ 
    -DCMAKE_INCLUDE_PATH=/usr/include/mkl ..\ 
    && make -j8 install \
    && make test 